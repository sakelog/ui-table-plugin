import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import './theme/theme/index.css';
import ElementUI from "element-ui";
import XEUtils from 'xe-utils'
import VXETable from "vxe-table";
import "vxe-table/lib/style.css";
import VueI18n from "vue-i18n"
import axios from 'axios'
import '@/icon'
import VueQuillEditor from 'vue-quill-editor'
import Quill from 'quill'
//图片修改大小
import imageResize from 'quill-image-resize-module'
//允许拖动图片到编辑器内
import { ImageDrop } from 'quill-image-drop-module'; // 拖动加载图片组件。
// main.js
import * as echarts from "echarts"

import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'

Vue.config.productionTip = false;

Vue.use(ElementUI);
Vue.use(VXETable);
Vue.use(VueI18n);
Vue.use(VueQuillEditor);

Vue.prototype.XEUtils = XEUtils
Vue.prototype.VXETable = VXETable
Vue.prototype.$http=axios;
Vue.prototype.$echarts = echarts

Quill.register('modules/imageResize', imageResize);
Quill.register('modules/imageDrop', ImageDrop);


const i18n=new VueI18n({
  // window.localStorage.getItem('language') == null ? "zh" : window.localStorage.getItem('language')
  locale: 'en',    // 语言标识
  messages: {
    'zh': require('./assets/lang/zh'),
    'en': require('./assets/lang/en')
  }
})

new Vue({
  router,
  store,
  i18n,
  render: (h) => h(App),
}).$mount("#app");
