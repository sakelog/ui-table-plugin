import Vue from 'vue'
import Router from 'vue-router'
import { getRoutes } from '@/api/role'
import NProgress from 'nprogress'

Vue.use(Router)

//静态路由,这里写好一些不需要从后台获取的路由,如首页,404页面
export const constantRoutes=[
    ...getRoutes,
    {
        path: '/login',
        component: () => import('@/views/login/login.vue'),
        hidden: true
    },
    {
        path: '/404',
        component: () => import('@/views/404.vue'),
        hidden: true
    }
]

const router=new Router({
    //刷新页面回到顶部位置
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes,
    mode:"history"
})

export default router;

/*
to:将要去的地址
from:从哪里来的地址
next:判断之后的回调，必须执行
*/
router.beforeEach((to,from,next)=>{
    NProgress.start();
    let token=localStorage.getItem("token");
    //判断token
    if(token){
      //token存在直接跳转页面 是否是去登录页
      if(to.path==='/login'){
        next('/home');
      }else{
        if (to.matched.length === 0) {
          next('/404') // 判断此跳转路由的来源路由是否存在，存在的情况跳转到来源路由，否则跳转到404页面
        }
        next();
      }
    }else{
      //token不存在，跳转到登录页，登陆后跳转到原来要去的页面
      if(to.path==='/login'){
        next();
      }else{
        next({
          path:'/login',
          query: {redirect: to.fullPath},// 将跳转的路由path作为参数，登录成功后跳转到该路由
        })
      }
    }
  })
  
  router.afterEach(()=>{
    NProgress.done();
  })
  