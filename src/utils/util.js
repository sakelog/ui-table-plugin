// 公用方法
/*
*千分位转化:最多只能保留三位小数
*var num = 123456789;num.toLocaleString();---"123,456,789"
*var num = 123456789.6785;num.toLocaleString();---"123,456,789.679"
*var num = 123456789.67435666666;num.toLocaleString();---"123,456,789.674"
*var num = 123456789.67;num.toLocaleString();---"123,456,789.67"
*/
export function changeNumber(num){
    return num.toLocaleString(); 
}