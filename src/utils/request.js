//请求封装
import axios from 'axios'
import { Message } from 'element-ui';

//创建实例
const service=axios.create({
    timeout:5000,//影响超时时间
    baseURL:process.env.VUE_APP_BASE_URL,//根据环境设置请求地址
})

//请求拦截
service.interceptors.request.use(config=>{
    //判断token是否存在
    let token=localStorage.getItem("token");
    if(token){
        config.headers.Authorization=token;
    }
    return config
},error=>{
    return Promise.reject(error);
})

//响应拦截
service.interceptors.response.use(response=>{
    //对不同相应状态码做处理
    const res=response.data;
    if(res.code==200){
        Promise.resolve(res);
    }else{
        Message.error(res.msg);
    }
    return res
},error=>{
    return Promise.reject(error)
})

export default service