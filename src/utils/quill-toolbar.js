export const toolbarList=[
  //文本对齐 - align 
  [{'align': []}],
  // 加粗 - bold； 斜体 - italic ；下划线 - underline ；删除线 - strike
  ['bold', 'italic', 'underline', 'strike'],
  //引用- blockquote； 代码块 - code-block
  ['blockquote','code-block'],
  //链接-link；图片 - image ；视频 - video ;公式 - formula 
  ['link', 'image', 'video','formula'],
  // 标题 - header 值字体大小，这种方式设置只有标题一和标题二样式
  [{'header': 1}, {'header': 2}],
  //有序列表/无需列表
  [{'list': 'ordered'}, {'list': 'bullet'}],
  // 上标/下标 - sub下标 super上标
  [{'script': 'sub'}, {'script': 'super'}],
  // 缩进 - indent 
  [{'indent': '-1'}, {'indent': '+1'}],
  // 文本方向 - direction 
  // [{'direction': 'rtl'}],
  //字体大小
  // [{'size': ['10px', '12px', '14px', '16px', '18px', '20px', '22px', '24px', '26px', '32px', '48px']}],
  [{ 'size': ['small', false, 'large', 'huge'] }],
  // 标题 - header 
  [{'header': [1, 2, 3, 4, 5, 6, false]}],
  // 颜色 - color ；背景颜色 - background
  [{'color': []}, {'background': []}],
  //字体
  [{'font': []}],
  // 清除字体样式
  ['clean']
]