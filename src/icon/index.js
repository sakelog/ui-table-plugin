import Vue from 'vue'//引入组件
import svgIcon from '@/components/svgIcon'
Vue.component('svg-icon', svgIcon)//此处递归获取.svg文件

const requireAll = requireContext => requireContext.keys().map(requireContext);
const req = require.context('./svg', false, /\.svg$/);
requireAll(req);