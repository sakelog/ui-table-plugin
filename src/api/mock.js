import Mock from 'mockjs'

//联级下拉框数据
// export let cascader=Mock.mock({
//     //cascaderList 数组名称 生成1-1000个数据
//     'cascaderList|1': [{
//         // 属性 id 是一个自增数，起始值为 1，每次增 1
//         'value|+1':1000,
//         'label|+1':1000,
//         'child|1000':[
//             {
//                 'value|+1': 1,
//                 'label|+1':1,
//             }
//         ]
//     }]
// });

//tableObj.tableData
export let tableData1=Mock.mock({
    'tableData1|7': [{
        'remark1|1-3': "备注1",
        'remark2|1-10': "备注2",
        'column1': "第一列",
        'column2': "第二列",
        'column3': "column3",
        'account|1-10': 10,
        'count|1-10': 10,
        'name':'@cname',
        'sex': "Man",
        'age|1-70': 68,
        'checked|1-2':true,
      },]
});

//tableObj2.tableData
export let tableData2=Mock.mock({
    'tableData2|10000': [{
        'col1': "col1",
        'col2': "col2",
        'col3': "col3",
        'col4': "col4",
        'col5': "col5",
        'col6': "col6",
        'col7': "col7",
        'col8': "col8",
        'col9': "col9",
        'col10': "col10",
        'col11': "col11",
        'col12': "col12",
        'col13': "col13",
        'col14': "col14",
        'col15': "col15",
        'col16': "col16",
        'col17': "col17",
        'col18': "col18",
        'col19': "col19",
        'col20': "col20",
        'col21': "col21",
        'col22': "col22",
      },]
});

//大数据下拉框
export let selectBigData=Mock.mock({
    'selectBigData|10000': [{
        'value|+1':1,
        'label':"@cname",
      },]
});

//联级选择
export const cascaderList=[
    {
        value: 'zhinan',
        label: '指南',
        children: [
            {value: '001',label: '001'}, 
            {value: '002',label: '002'}, 
            {value: '003',label: '003'},
            {value: '004',label: '004'},
            {value: '005',label: '005'}, 
            {value: '006',label: '006'},
            {value: '007',label: '007'},
            {value: '008',label: '008'}, 
            {value: '009',label: '009'}, 
            {value: '010',label: '010'}, 
            {value: '011',label: '011'},
            {value: '012',label: '012'},
            {value: '013',label: '013'}, 
            {value: '014',label: '014'},
            {value: '015',label: '015'},
            {value: '016',label: '016'}, 
            {value: '017',label: '017'}, 
            {value: '018',label: '018'}, 
            {value: '019',label: '019'},
            {value: '020',label: '020'},
            {value: '021',label: '021'}, 
            {value: '022',label: '022'},
            {value: '023',label: '023'},
            {value: '024',label: '024'}, 
            {value: '025',label: '025'},
            {value: '026',label: '026'}, 
            {value: '027',label: '027'}, 
            {value: '028',label: '028'}, 
            {value: '029',label: '029'},
            {value: '030',label: '030'},
        ]
    }
]

