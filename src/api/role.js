export const getRoutes=[
    {
        path: '',
        component: () => import('@/layout/index'),
        name:'Layout',
        redirect: '/home',
        children:[
            {
                path: '/home',
                component: ()=>import('@/views/home'),
                meta:{
                    icon:'el-icon-s-home',
                    title:'首页',
                }
            },
        ]
    },
    {
        path: '/map',
        component: () => import('@/layout/index'),
        children: [
            {
                path: '/map/map',
                component: ()=>import('@/views/map/map'),
                name: 'map',
                meta: {
                    title: '百度地图案例',
                    icon: 'el-icon-location',
                    // roles: ['admin']
                }
            },
        ],
        meta: {
            title: '地图',
            icon: 'el-icon-menu',
            // roles: ['admin']
        },
    },
    {
        path: '/form',
        component: () => import('@/layout/index'),
        children: [
            {
                path: '/form/form',
                component: ()=>import('@/views/form/form'),
                name: 'form',
                meta: {
                    title: 'form表单',
                    icon: 'el-icon-folder-checked',
                    // roles: ['admin']
                }
            },
        ],
        meta: {
            title: '表单',
            icon: 'el-icon-menu',
            // roles: ['admin']
        },
    },
    {
        path: '/table',
        component: () => import('@/layout/index'),
        children: [
            {
                path: '/table/commonTable',
                component: ()=>import('@/views/table/commonTable'),
                name: 'commonTable',
                meta: {
                    title: '普通表格',
                    icon: 'el-icon-film',
                    // roles: ['admin']
                }
            },
            {
                path: '/table/treeTable',
                component: ()=>import('@/views/table/treeTable'),
                name: 'treeTable',
                meta: {
                    title: '树表格',
                    icon: 'el-icon-film',
                    // roles: ['admin']
                }
            },
            {
                path: '/table/bigDataTable',
                component: ()=>import('@/views/table/bigDataTable'),
                name: 'bigDataTable',
                meta: {
                    title: '大数据表格',
                    icon: 'el-icon-film',
                    // roles: ['admin']
                }
            },
        ],
        meta: {
            title: 'table表格',
            icon: 'el-icon-menu',
            // roles: ['admin']
        },
    },
    {
        path: '/loading',
        component: () => import('@/layout/index'),
        children: [
            {
                path: '/loading/loading',
                component: ()=>import('@/views/loading/loading'),
                name: 'map',
                meta: {
                    title: 'loading效果',
                    icon: 'el-icon-refresh',
                    // roles: ['admin']
                }
            },
        ],
        meta: {
            title: 'loading',
            icon: 'el-icon-menu',
            // roles: ['admin']
        },
    },
    {
        path: '/richText',
        component: () => import('@/layout/index'),
        children: [
            {
                path: '/richText/text',
                component: ()=>import('@/views/richText/quill'),
                name: 'Text',
                meta: { 
                    title: 'quill富文本', 
                    icon:'el-icon-s-order', 
                    // roles: ['admin']
                }
            },
            {
                path: '/richText/tinymce',
                component: ()=>import('@/views/richText/tinymce'),
                name: 'Tiny',
                meta: { 
                    title: 'tinymce富文本', 
                    icon:'el-icon-document-copy', 
                    // roles: ['admin']
                }
            }
        ],
        meta:{
            icon:'el-icon-notebook-1',
            title:'富文本',
        },
    },
]
  