const path = require("path");
const resolve = (dir) => path.join(__dirname, dir);
var webpack = require('webpack');
const BundleAnalyzerPlugin=require('webpack-bundle-analyzer').BundleAnalyzerPlugin

module.exports = {
  //baseUrl: './', // 默认'/'，部署应用包时的基本 URL
  productionSourceMap: false, // 生产环境的 source map
  lintOnSave: false,
  devServer: {
    //运行时去掉eslint语法警告
    overlay: {
      warnings: false,
      errors: false,
    }, 
    // disableHostCheck: true,
    port: "8000",
    //跨域代理
    // proxy: {
    //   "/api": {
    //     target: "http://localhost:3000/",
    //     changeOrigin: true,
    //     //重写匹配的字段，如果不需要在请求路径上，重写为""
    //     pathRewrite: {  
    //       "^/api": "/"
    //     }
    //   },
    // },
  },
  chainWebpack: (config) => {
    config.resolve.symlinks(true); //热更新
    config.resolve.alias.set("@", resolve("src")); //添加别名
    //svg loader
    const svgRule = config.module.rule('svg')   
    svgRule.uses.clear()    
    svgRule        
      .test(/\.svg$/)        
      .include.add(path.resolve(__dirname, './src/icon')).end()        
      .use('svg-sprite-loader')        
      .loader('svg-sprite-loader')        
      .options({          
        symbolId: 'icon-[name]'        
      });   
  },
  configureWebpack:{
    plugins:[
      new webpack.ProvidePlugin({
        'window.Quill':'quill/dist/quill.js',
        'Quill':'quill/dist/quill.js'
      }),
      new BundleAnalyzerPlugin({
          analyzerMode: 'server',
          analyzerHost: '127.0.0.1',
          analyzerPort: 8889,
          reportFilename: 'report.html',
          defaultSizes: 'parsed',//默认显示在报告中的模块大小匹配方式。应该是stat，parsed或者gzip中的一个。
          openAnalyzer: true,//在默认浏览器中自动打开报告。
          generateStatsFile: false,//如果为true，则Webpack Stats JSON文件将在bundle输出目录中生成。
          statsFilename: 'stats.json',//相对于捆绑输出目录。
          statsOptions: null,
          logLevel: 'info',//日志级别，可以是info, warn, error, silent。
        }),
    ],
    externals:{
      "BMap":"BMap"
    }
  }
};
